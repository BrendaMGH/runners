<?php
	// //Create table
	// $conn = new mysqli('URL', 'USER', 'PASSWORD', 'DATABASE');
	// if ($conn->connect_error) {
	// 	die("Connection failed: " . $conn->connect_error);
	// }
	// $sql = "CREATE TABLE runners(
	// 	runner_id INT not null AUTO_INCREMENT,
	// 	first_name VARCHAR(100) not null,
	// 	last_name VARCHAR(100) not null,
	// 	gender VARCHAR(1) not null,
	// 	finish_time VARCHAR(10),
	// 	PRIMARY KEY (runner_id)
	// )";
	// if ($conn->query($sql) === TRUE) {
	// 	echo "Table MyGuests created successfully";
	// } else {
	// 	echo "Error creating table: " . $conn->error;
	// }  
	// $conn->close();

	if($_POST){	
		if ($_POST['action'] == 'addRunner') {
			// Se obtienen los datos del formulario
			$fname = htmlspecialchars($_POST['txtFirstName']);
			$lname = htmlspecialchars($_POST['txtLastName']);
			$gender = htmlspecialchars($_POST['ddlGender']);
			$minutes = htmlspecialchars($_POST['txtMinutes']);
			$seconds = htmlspecialchars($_POST['txtSeconds']);

			//Validacion
			if(preg_match('/[^\w\s]/i', $fname) || preg_match('/[^\w\s]/i', $lname)) {
				fail('Invalid name provided.');
			}
			if( empty($fname) || empty($lname) ) {
				fail('Please enter a first and last name.');
			}
			if( empty($gender) ) {
				fail('Please select a gender.');
			}
			if( empty($minutes) || empty($seconds) ) {
				fail('Please enter minutes and seconds.');
			}
			
			$time = $minutes.":".$seconds;

			//Se agrega el corredor a la base de datos
			$query = "INSERT INTO runners SET first_name='$fname', last_name='$lname', gender='$gender', finish_time='$time'";
			$result = db_connection($query);
			
			if ($result) {
				$msg = "Runner: ".$fname." ".$lname." added successfully" ;
				success($msg);
			} else {
				fail('Insert failed.');
			}
			exit;
		}
	}

	if($_GET){

		if($_GET['action'] == 'getRunners'){
			//Se obtienen la informacion de los corredores de la base de datos
			$query = "SELECT first_name, last_name, gender, finish_time FROM runners order by finish_time ASC ";
			$result = db_connection($query);
			$runners = array();
			// Se guarda toda la informacion en un unico arreglo y se convierte a JSON
			while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
				array_push($runners, array('fname' => $row['first_name'], 'lname' => $row['last_name'], 'gender' => $row['gender'], 'time' => $row['finish_time']));
			}
			echo json_encode(array("runners" => $runners));
			exit;
		}
	}
		
	//Funcion para conectarse con la base de datos
	function db_connection($query) {
		$connection = mysqli_connect('URL', 'USER', 'PASSWORD', 'DATABASE');
		if (!$connection) {
			fail("Could not connect to database");
		}
		return mysqli_query($connection, $query);
	}
	
	//Funcion para indicar error
	function fail($message) {
		die(json_encode(array('status' => 'fail', 'message' => $message)));
	}
	//Funcion para indicar exito
	function success($message) {
		die(json_encode(array('status' => 'success', 'message' => $message)));
	}
?>