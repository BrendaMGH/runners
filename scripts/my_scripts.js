$(document).ready(function () {
  var FREQ = 10000;
  var repeat = true;

  //Iniciar las funciones
  showFrequency();
	getDBRacers();
	startAJAXcalls();

  //Eventos para iniciar o parar las actualizaciones
  $("#btnStop").click(function () {
    repeat = false;
    $("#freq").html("Updates paused.");
  });
  $("#btnStart").click(function () {
    repeat = true;
    startAJAXcalls();
    showFrequency();
  });

  //Evento para agregar un corredor
  $('#btnSave').click(function() {
		var data = $("#addRunner :input").serializeArray();
		$.post($("#addRunner").attr('action'), data, function(json){
			
			if (json.status == "fail") {
				alert(json.message);
			}
			if (json.status == "success") {
				alert(json.message);
				clearInputs();
			}
		}, "json");

	});	

  $("#addRunner").submit(function () {
    return false;
  });

  //////////////////////////////////////Functions//////////////////////////////////////
  //Self-call function
  function startAJAXcalls() {
    if (repeat) {
      setTimeout(function () {
        getDBRacers();
        startAJAXcalls();
      }, FREQ);
    }
  }

  //Se obtienen los corredores de la base de datos
  function getDBRacers(){
    $.getJSON("../PHP/service.php?action=getRunners", function(json) {
      console.log(json.runners.length);
      if (json.runners.length > 0) {
        $('#finishers_m').empty();
        $('#finishers_f').empty();
        $('#finishers_all').empty();
        $.each(json.runners,function() {
          var info = '<li>Name: ' + this['fname'] + ' ' + this['lname'] +
          '. Time: ' + this['time'] + '</li>';
          if(this['gender'] == 'm'){
            $('#finishers_m').append(info);
          }else if(this['gender'] == 'f'){
            $('#finishers_f').append(info);
          }else{}
            $('finishers_all').append(info);
        });
      }
    });
    getTimeAjax();
  }

  //Se limpian las entradas del formulario
  function clearInputs() {
    $("#addRunner :input").each(function () {
      $(this).val("");
    });
  }

  //Se muestra la frecuancia de las actualizaciones
  function showFrequency() {
    $("#freq").html("Page refreshes every " + FREQ / 1000 + " second(s).");
  }

  //Se muestra la fecha de la actualizacion con PHP
  function getTimeAjax() {
    $("#updatedTime").load("../PHP/time.php");
  }

  //Se obtienen los corredores del archivo XML
  function getXMLRacers() {
    $.ajax({
      url: "../SERVER/finishers.xml",
      cache: false,
      dataType: "xml",
      success: function (xml) {
        $("#finishers_m").empty();
        $("#finishers_f").empty();
        $("#finishers_all").empty();
        $(xml)
          .find("runner").each(function () {
            var info = "<li>Name: " + $(this).find("fname").text() + " " + $(this).find("lname").text() +
              ". Time: " + $(this).find("time").text() + "</li>";
            if ($(this).find("gender").text() == "m") {
              $("#finishers_m").append(info);
            } else if ($(this).find("gender").text() == "f") {
              $("#finishers_f").append(info);
            } else {}
            $("#finishers_all").append(info);
          });
        getTimeAjax();
      },
    });
  }

  //Funcion vieja para mostrar la hora de las actualizaciones
  function getTime() {

    var a_p = "";
    var d = new Date();
    var curr_hour = d.getHours();

    curr_hour < 12 ? (a_p = "AM") : (a_p = "PM");
    curr_hour == 0 ? (curr_hour = 12) : (curr_hour = curr_hour);
    curr_hour > 12 ? (curr_hour = curr_hour - 12) : (curr_hour = curr_hour);

    var curr_min = d.getMinutes().toString();
    var curr_sec = d.getSeconds().toString();

    if (curr_min.length == 1) {
      curr_min = "0" + curr_min;
    }
    if (curr_sec.length == 1) {
      curr_sec = "0" + curr_sec;
    }

    $("#updatedTime").html(
      curr_hour + ":" + curr_min + ":" + curr_sec + " " + a_p
    );
  }
});
